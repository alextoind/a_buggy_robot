# A Buggy Robot Problem Description
This package contains only a simple robot (2 links and 1 joint). It's aim is to show that when parsing the URDF model with Gazebo6 (*sdformat3*) or Gazebo7 (*sdformat4*), **all the elements under `<gazebo>` tags which would end under the `<visual>` or `<collision>` SDF elements are not parsed at all.**

In the `robot_description/urdf/` directory can be found the URDF/XACRO robot model and the SDF parsed versions using respectively Gazebo5, Gazebo6 and Gazebo7. Only the `parsed_gazebo5.sdf` is correctly visualized in Gazebo.

From Gazebo6 on (i.e. using *sdformat3* and *sdformat4*), `<visual>` and `<collision>` elements get augmented unique names when they are converted to SDF (which differ from those given in the URDF). I think that the parser tries to use the URDF visual/collision names but they are not used anymore, e.g. `visual_waist` and `collision_waist` are the name in the URDF model and the respectively SDF ones are `waist_fixed_joint_lump__visual_waist_visual` and `waist_fixed_joint_lump__collision_waist_collision`.

## Diff Example
Here is an extract of the difference between parsing with Gazebo5 and with Gazebo7 (the full difference and the one w.r.t. Gazebo 6 can be found in `robot_description/urdf/`):

    85c71
    <       <collision name='pelvis_collision'>
    ---
    >       <collision name='pelvis_fixed_joint_lump__collision_pelvis_collision'>
    92,108d77
    <         <surface>
    <           <contact>
    <             <ode>
    <               <kp>1e+06</kp>
    <               <kd>100</kd>
    <               <max_vel>1</max_vel>
    <               <min_depth>0</min_depth>
    <             </ode>
    <           </contact>
    <           <friction>
    <             <ode>
    <               <mu>1.5</mu>
    <               <mu2>1.5</mu2>
    <               <fdir1>0 0 1</fdir1>
    <             </ode>
    <           </friction>
    <         </surface>
    110c79
    <       <visual name='pelvis_visual'>
    ---
    >       <visual name='pelvis_fixed_joint_lump__visual_pelvis_visual'>
    117,122d85
    <         <material>
    <           <script>
    <             <name>Gazebo/Black</name>
    <             <uri>__default__</uri>
    <           </script>
    <         </material>

## Notes
This package has been test only with ROS Jade. I don't know if ROS Indigo shows the same behaviour or not.